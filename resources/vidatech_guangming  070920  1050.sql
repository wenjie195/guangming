-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2020 at 04:49 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_guangming`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `uid`, `title`, `content`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '71dbfdf1997603d9610a3f9f90c45a1b', NULL, 'Announcement: We will have a live talk in 31st August 2020. Stay Tuned!', 'Available', 2, '2020-07-21 03:46:13', '2020-07-21 03:52:52'),
(2, '1362413c4842c918c87c09917a7a6227', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 1', 'Available', 1, '2020-07-21 03:46:39', '2020-07-21 03:46:39'),
(3, '5d4ad3b1b412f1aa01f73a5d4fb57c6f', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 2\r\n', 'Available', 1, '2020-07-21 03:46:46', '2020-07-21 03:46:46'),
(4, '6eb492ce0f4fe93827ee5d9c7ae882b6', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 3', 'Available', 1, '2020-07-21 03:46:51', '2020-07-21 03:46:51'),
(5, 'd14db52f8c57b997cef7c032bfdd323a', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 4\r\n', 'Available', 1, '2020-07-21 03:53:07', '2020-07-21 03:53:07'),
(6, '0cc59292c2308b7d2d41ef9cdcfe0d2e', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 5', 'Available', 1, '2020-07-21 03:53:14', '2020-07-21 03:53:14'),
(7, '6d30e124799c7d79293f91f5a60e393d', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 6', 'Available', 1, '2020-07-21 03:53:20', '2020-07-21 03:53:20'),
(8, '016d2bf60f79ce76cffa816301bc1ca3', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 7\r\n', 'Available', 1, '2020-07-21 03:53:34', '2020-07-21 03:53:34'),
(9, '422644553d984015b2ebd8d1f828e4ab', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 8', 'Available', 1, '2020-07-21 03:53:40', '2020-07-21 03:53:40');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `link_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `link_two` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `uid`, `user_uid`, `image_one`, `link_one`, `image_two`, `link_two`, `type`, `date_created`, `date_updated`) VALUES
(1, '9a2257c9de18c44cabd3bb8a9ed46516', '923a1263cf3818339855fcf8f37cbef2', '1595304402.png', 'https://www.google.com/', NULL, NULL, 1, '2020-07-21 04:06:42', '2020-07-21 04:06:57'),
(2, '33db1d4c1546e4131d54e06a308bfb3f', '923a1263cf3818339855fcf8f37cbef2', '1595304434.png', 'http://www.ixftv.com/', NULL, NULL, 1, '2020-07-21 04:07:14', '2020-07-21 04:07:16'),
(3, '5ce0fe981215364c1fdc2bc5c33aff7a', '923a1263cf3818339855fcf8f37cbef2', '1595304446.png', 'https://www.amway.my/', NULL, NULL, 1, '2020-07-21 04:07:26', '2020-07-21 04:07:36'),
(4, 'b1730bcc9c1e0b9b8105099d06b9a0c6', '923a1263cf3818339855fcf8f37cbef2', '1595304474.png', 'https://guangming.com.my/', NULL, NULL, 1, '2020-07-21 04:07:54', '2020-07-21 04:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `live_share`
--

CREATE TABLE `live_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `live_share`
--

INSERT INTO `live_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'dc101fe3faae9ca45d57f5e4c9c05dcd', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Main Video', 'Ewein Host 1', 'Youtube', 'mw90POFCMUQ', 'Available', 1, '2020-07-10 01:45:17', '2020-07-10 01:45:17');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `reply_one` varchar(255) DEFAULT NULL,
  `reply_two` varchar(255) DEFAULT NULL,
  `reply_three` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `uid`, `username`, `message_uid`, `receive_message`, `reply_message`, `reply_one`, `reply_two`, `reply_three`, `user_status`, `admin_status`, `date_created`, `date_updated`) VALUES
(1, '601caafb289331ee966ddde998fe2243', 'user', '9e548a6dbd0f85525968e1aa3047e429', '123321', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 02:43:14', '2020-08-13 02:43:14'),
(2, '601caafb289331ee966ddde998fe2243', 'user', '6b4c7116d5a724ae390ecfbce480feb7', 'asddsa', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 02:43:19', '2020-08-13 02:43:19'),
(3, '601caafb289331ee966ddde998fe2243', 'user', '1c00779e44ab4dccde8d81d8c24c844c', '123321', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 02:44:05', '2020-08-13 02:44:05'),
(4, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', '9b7dcacdcac43b82329c7139fa413a11', 'asdddd', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 02:44:46', '2020-08-13 02:44:46'),
(5, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', '1c512ddee81cb92aaee917d97ac677b6', 'dddd', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 02:44:49', '2020-08-13 02:44:49'),
(6, '601caafb289331ee966ddde998fe2243', 'user', '1209c46d0571883db3816fbc11102fff', 'assad', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 04:38:45', '2020-08-13 04:38:45'),
(7, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', '4ea162f9e1a6d61b974f7a423e8f076b', 'sdasdasd', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 04:39:10', '2020-08-13 04:39:10'),
(8, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', '50c7ad29efa43cfb24bb8aea58a41afc', 'vvvvv', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 04:39:14', '2020-08-13 04:39:14'),
(9, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', 'a79122b2b6ab0221118784fa7c380b77', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 04:40:16', '2020-08-13 04:40:16'),
(10, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', '5f9b03f995121906a667493a57a1164f', 'asddddd', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 04:40:21', '2020-08-13 04:40:21'),
(11, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', 'aa981366a5a3b5aaa9e21079612073aa', 'ddddd', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 04:40:23', '2020-08-13 04:40:23');

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `platform`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Youtube', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'Facebook', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'Zoom', 'Stop', 2, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'UStream', 'Stop', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '601caafb289331ee966ddde998fe2243', 'user', 'user@gmail.com', '16858628f03e0be66f5fa868d2a5b961a00a9f382934b1c027481e00ab015c1d', '1c31c05f74cc2d64c2236039a7ef50bc8e114b5a', '0123456', NULL, 1, '2020-08-13 01:58:38', '2020-08-13 01:58:38'),
(2, 'df2bbb6ef3a445de22165f32ce23d85b', 'user2', 'user2@gmail.com', '9d7be70c67312b98473f6c67486afe4b1f6306e1a0be6a1e5f4e313d059843f1', '187f5be44f42f44cf5c483ccc508292ed3bef243', '2456789', NULL, 1, '2020-08-13 02:43:57', '2020-08-13 02:43:57');

-- --------------------------------------------------------

--
-- Table structure for table `sharing`
--

CREATE TABLE `sharing` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sharing`
--

INSERT INTO `sharing` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `remark`, `file`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'b2c4b2dc1e1e9042aeb8013d3edb55f9', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop Zoom 1', 'Ewein Zoom Host 1', 'Zoom', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Remark Zoom 1', '1594782480WeChat Image_20200715110013.jpg', 'Delete', 3, '2020-07-15 03:08:00', '2020-07-15 03:08:00'),
(2, 'cc368c9e87356950393895933447b7d1', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop Zoom 2', 'Ewein Zoom Host 2', 'Zoom', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Remark Zoom 2', '1594782521staff1.jpg', 'Available', 1, '2020-07-15 03:08:41', '2020-07-15 03:08:41'),
(3, '901b7bec4fc407212b9ecdc751a9ae81', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop Zoom 3', 'Ewein Zoom Host 3', 'Zoom', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Remark Zoom 3', '1594782552staff2.jpg', 'Available', 1, '2020-07-15 03:09:12', '2020-07-15 03:09:12'),
(4, '4ce6604fcf9bca71e9bac5779d0f9c93', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop Zoom 4', 'Ewein Zoom Host 4', 'Zoom', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Remark Zoom 4', '15947825821.jpg', 'Available', 1, '2020-07-15 03:09:42', '2020-07-15 03:09:42'),
(5, '91d160a7474e2623b16de57f157f5cad', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop Zoom 5', 'Ewein Zoom Host 5', 'Zoom', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Remark Zoom 5', '15947826042.jpg', 'Available', 1, '2020-07-15 03:10:04', '2020-07-15 03:10:04'),
(6, '751c1d4783bac7c62739b7d631d09ad4', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop Zoom 6', 'Ewein Zoom Host 6', 'Zoom', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Remark Zoom 6', '15947828933.jpg', 'Available', 1, '2020-07-15 03:14:53', '2020-07-15 03:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `sub_share`
--

CREATE TABLE `sub_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `title_two` varchar(255) DEFAULT NULL,
  `host_two` varchar(255) DEFAULT NULL,
  `platform_two` varchar(255) DEFAULT NULL,
  `link_two` text DEFAULT NULL,
  `remark_two` text DEFAULT NULL,
  `file_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_share`
--

INSERT INTO `sub_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `remark`, `file`, `title_two`, `host_two`, `platform_two`, `link_two`, `remark_two`, `file_two`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '51bc8cb546ab7a2a4a5f2bbf18910178', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop 1', 'Ewein Host 1', 'Youtube', 's3E77kvWQdU', 'Remark 1', '1594780461Crypto-pages.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-07-15 02:34:21', '2020-07-15 02:34:21'),
(2, '653d20c078097b48a71b89c6601655f8', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop 2', 'Ewein Host 2', 'Youtube', 'yX6qBM6Wskk', 'Remark 2', '15947806412019-12-03_12-58-21_winscan_to_pdf.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-07-15 02:37:21', '2020-07-15 02:37:21'),
(3, 'b11180ba8c1dc483f7ad6a49e8610e5c', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop 3', 'Ewein Host 3', 'Youtube', '705UE8Btco8', 'Remark 3', '', NULL, NULL, NULL, NULL, '176504640005524', NULL, 'Available', 1, '2020-07-15 02:42:45', '2020-07-15 02:42:45'),
(4, 'c3fa463e97a2b5992aa2bb1add91dbfc', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop 4', 'Ewein Host 4', 'Youtube', '36h0-Z6KbL0', 'Remark 4', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-07-15 02:44:11', '2020-07-15 02:44:11'),
(5, 'ffc79081eec3305727a724763103fe3f', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop 5', 'Ewein Host 5', 'Youtube', 'beWjTKhY_l8', 'Remark 5', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-07-15 02:44:48', '2020-07-15 02:44:48'),
(6, '782d1242f00e93c910fee306e4649cb1', '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'Ewein Prop 6', 'Ewein Host 6', 'Youtube', 'RwCTmrXYR7c', 'Remark 6', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-07-15 02:45:19', '2020-07-15 02:45:19');

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE `title` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `title`
--

INSERT INTO `title` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'WELCOME TO LIVE-STREAMING PLATFORM', 'Available', 1, '2020-07-08 01:18:45', '2020-07-08 01:18:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `broadcast_live` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `title` varchar(255) DEFAULT NULL,
  `broadcast_share` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `autoplay` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `nationality`, `broadcast_live`, `title`, `broadcast_share`, `platform`, `link`, `autoplay`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', '6012-3456789', NULL, NULL, NULL, NULL, NULL, 'Youtube', 'zpH9UZrU6h4', NULL, 1, 0, '2020-03-18 06:28:23', '2020-07-10 01:50:21'),
(2, '923a1263cf3818339855fcf8f37cbef2', 'Ewein Properties', 'ewin@gmail.com', 'abcc2bf9cc66d22e59b6a022a43ad5f71d7075ee038673c351e48202e62228d7', '9e4df10acd98eeda75145c8a837d90ab5e9cf2bd', '123123123', NULL, NULL, 'Available', NULL, '1594345769', 'Youtube', 'r0DHed8buKs', NULL, 1, 1, '2020-07-10 01:14:42', '2020-07-10 02:01:58');

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userdata`
--

INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(1, 'fdc700332b8cdf88e179d60fe34d9ba8', 'Lim Theam Huat', 'limtheamhuat2012@gmail.com', 'f059d809ce946ece99b1525157ffe4d28e2643146bb1733d772ada4dd42ca921', '3a99e14af2b1112c3c9c3d27edc9fe65eab9df17', '0162319397', NULL, 1, '2020-09-07 02:41:18', '2020-09-07 02:41:18'),
(2, 'aa990e0054a9091ba74b2bc00b547c7d', 'Simon Lim', 'limlikean@gmail.com', 'f01bc863c2f05b851cd34e006779f4d513e78500bd5d51f8fa0e99567bddb9ff', 'c68fb9d59e7beee87e12966ea88959e39234fca7', '0194459405', NULL, 1, '2020-09-07 02:41:47', '2020-09-07 02:41:47'),
(3, '343412e6a1d5570d6f49d8c64f247a79', 'David', 'davidyewab@gmail.com', 'bf050a0ac92e494da2f2d4ebba7f1aa191f15b9a9c59536cf3c6b1a38e2b9697', '155c36a59422be9ee6c08f909f1cc2599b02ad4b', '0164232203', NULL, 1, '2020-09-07 02:42:15', '2020-09-07 02:42:15'),
(4, '69229a4ddbad02a7cc3ccbd0e092129d', 'Chuzen Lee', 'leechuzen@yahoo.com', 'b856b153a7f0132aa1b1ce513ff733ad0f8b35f7362c02792c412b45e1cfba85', '2992d25615c7170e914b84c7b34e32df158d9cae', '0164744193', NULL, 1, '2020-09-07 02:42:30', '2020-09-07 02:42:30'),
(5, 'eb7fc103524b6801c3116bfd845e2887', 'Cheah Yi Wei', 'cheahywei@hotmail.com', '7819b95ac947036e92be5243c739d011cea11793d61d95e993477b5441939cd4', '6fe8a3c7d441c2aa075d9d6eb60d3ccaa791d213', '0125280869', NULL, 1, '2020-09-07 02:42:44', '2020-09-07 02:42:44'),
(6, 'ab13945019ad09c9fca6ab5adc488fbc', 'Cheah Chin Wah', 'majorcheah@gmail.com', 'da1b5486b3960e2a465519ee449c479a41a66bbbd8c21c49d0a58d648499b4dc', 'b2663843dda8d63d760490c107287aef14ce7542', '0124280869', NULL, 1, '2020-09-07 02:42:58', '2020-09-07 02:42:58'),
(7, 'cf0a947e487d19ffb5b45b1d4c57215c', 'Jimmy Ng Leong heng', 'Jimnglh8@gmail.com', '8212ef6a99bb55a6c1d9254280b6ba15c1900f7ea94a6851b0e23aeae5101d48', '7745648360f4f78df8d5a708241b942efed5aad2', '0125590999', NULL, 1, '2020-09-07 02:43:12', '2020-09-07 02:43:12'),
(8, 'bacdaa77dbf496cb1498491192035f12', 'TIU KOK KEI', 'tiukk@yahoo.com', '9007342cca89ca75171e30f0f5eba723a326c793a1e9077adba5f05157626fe9', '50a879acfb93ed1512a7c7acc6bdf3acd668bb4f', '0174167942', NULL, 1, '2020-09-07 02:43:32', '2020-09-07 02:43:32'),
(9, '0bf8570a61a9efaee2853a800f00d092', 'Leena Lee', 'lee@wellpoint.com.my', '9938433fddab3472e79519a4e95941ee069acfc095d538d097048512366ea73c', 'eaf0a8f6cf9c4ca91e096f79dae789ccb6b67a3f', '0124775428', NULL, 1, '2020-09-07 02:43:45', '2020-09-07 02:43:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_share`
--
ALTER TABLE `live_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `sharing`
--
ALTER TABLE `sharing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_share`
--
ALTER TABLE `sub_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `title`
--
ALTER TABLE `title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `live_share`
--
ALTER TABLE `live_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `platform`
--
ALTER TABLE `platform`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sharing`
--
ALTER TABLE `sharing`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_share`
--
ALTER TABLE `sub_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `title`
--
ALTER TABLE `title`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `userdata`
--
ALTER TABLE `userdata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
