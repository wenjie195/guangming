<?php
// DB SQL Connection
function connDB(){
    // Create connection
    // $conn = new mysqli("localhost", "root", "","vidatech_livestream");//for localhost
    $conn = new mysqli("localhost", "root", "","vidatech_guangming");//for localhost

    // $conn = new mysqli("localhost", "gmveccom_gmuser", "5bM1o9UmQ4Nm","gmveccom_live"); //for live server on 070920 1009
    // $conn = new mysqli("localhost", "gmveccom_testone", "JPtj5WQqYvmT","gmveccom_test"); //for test server
    // $conn = new mysqli("localhost", "gmveccom_user", "Oc[gTVUb4P3a","gmveccom_livestream"); //for live server
    // $conn = new mysqli("localhost", "gmveccom_dupuser", "Am_[o1(l[iQf","gmveccom_duplicate"); //for guangming duplicate
    // $conn = new mysqli("localhost", "gmveccom_tester", "BCHu0THxmusw","gmveccom_testing"); //for gmvec testing

    // $conn = new mysqli("localhost", "dxforext_lives", "hlH4GNV9YjI1","dxforext_livestream"); //for live server
    // $conn = new mysqli("localhost", "dxforext_streamuser", "q2ze39lWkQRk","dxforext_streaming"); //for live server
    // $conn = new mysqli("localhost", "ichibang_stream", "DojXbTgr9j4U","ichibang_livestream"); //for live server

    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
