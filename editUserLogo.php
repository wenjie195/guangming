<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/editUserLogo.php" />
<meta property="og:title" content="User Logo | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>User Logo | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/editUserLogo.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">User Logo</h2> 
    
    <div class="clear"></div>
        <div>

            <?php
            if(isset($_POST['user_uid']))
            {
            $conn = connDB();
            $userLogos = getImage($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
            $userLogo = $userLogos[0];
            $uid = ($_POST['user_uid']);
            $pictures = $userLogo -> getImageOne();
                if($pictures)
                {
                    ?>
                    <div class="edit-profile-div-smalltj text-center white-text clean">
                        <img src="userLogo/<?php echo $pictures?>" class="imagetj camera-icon overwrite-imagetj" alt="Update Profile Picture" title="Update Profile Picture">
                        </br>
                        <form method="POST" action="uploadLogo.php">
                            <button class="edit-profile-pic-btn text-center white-text clean ow-update-pic-btn" type="submit" name="picture_uid" value="<?php echo $uid;?>"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"></button>
                        </form>
                    </div>
                    
                    <!--for($cnt = 0;$cnt < count($userLogo) ;$cnt++)
                    {
                    ?>
                         <a href="https://ecoworld.my/ecoforest/" target="_blank"><img src="img/logo-03.png" class="first-logo project-logo opacity-hover"></a>
                        <a href="https://ecoworld.my/ecohorizon/" target="_blank"><img src="img/logo-04.png" class="second-logo project-logo opacity-hover"></a> -->
                     <?php
                    // }
                     ?>
                <?php
                }
                else
                {
                ?>
                    <div class="edit-profile-div-smalltj text-center white-text clean">
                        <form method="POST" action="uploadLogo.php">
                            <button class="edit-profile-pic-btn text-center white-text clean ow-update-pic-btn" type="submit" name="picture_uid" value="<?php echo $uid;?>"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"></button>
                        </form>
                    </div>
                <?php
                }
            }
            ?>
        </div>

    <div class="clear"></div>
       
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>