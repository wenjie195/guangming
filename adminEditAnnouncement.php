<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminEditAnnouncement.php" />
<meta property="og:title" content="Edit Announcement | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Edit Announcement  | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminEditAnnouncement.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
	<div class="mid-width">
        <h2 class="h1-title">Edit Announcement</h2>
            <div class="clear"></div>
    
            <form method="POST" action="utilities/adminEditAnnouncementFunction.php">
    
                <?php
                if(isset($_POST['announcement_uid']))
                {
                    $conn = connDB();
                    $announcementContent = getAnnouncement($conn,"WHERE uid = ? ", array("uid") ,array($_POST['announcement_uid']),"s");
                ?>
    
                    <div class="width100">
                       <!-- <p class="input-top-text">Announcement Content</p>-->
                        <textarea class="aidex-input clean ann-min-height" type="text" placeholder="Announcement Content" id="update_content" name="update_content" required><?php echo $announcementContent[0]->getContent();?></textarea>        
                    </div> 
                    
                    <div class="clear"></div>  
    
                    <input type="hidden" value="<?php echo $announcementContent[0]->getUid();?>" name="announcement_uid" id="announcement_uid" readonly> 
    
                <?php
                }
                ?>
            
                <div class="clear"></div>  
    
                <div class="width100 overflow text-center">     
                    <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
                </div>
    
            </form>

	</div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>