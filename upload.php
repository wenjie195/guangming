<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];
	$uid = $_POST['picUid'];
	
	// $userUid = $_POST['user_uid'];
	// $picLinkUid = $_POST['pic_link_uid'];
	// $picValue = $_POST['pic_value'];

	$image_array_1 = explode(";", $data);
	$image_array_2 = explode(",", $image_array_1[1]);
	$data = base64_decode($image_array_2[1]);
	$imageName = time() . '.png';
	// file_put_contents('userLogo/'.$imageName, $data);
	file_put_contents('uploads/'.$imageName, $data);


	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		if($imageName)
		{
				array_push($tableName,"image_one");
				array_push($tableValue,$imageName);
				$stringType .=  "s";
		}
		array_push($tableValue,$uid);
		$stringType .=  "s";
		$withdrawUpdated = updateDynamicData($conn,"image"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		if($withdrawUpdated)
		{
		?>	
			<div>
				<img src=uploads/<?php echo $imageName  ?> />
			</div>	
			<h4><?php echo "Successfully Upload Profile Image"; ?></h4>
			<!-- <form action="./adminDashboard.php"> -->
			<a href="adminDashboard.php">
				<div class="width100 overflow text-center">     
					<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
				</div>
			</a>
		<?php
		}
		else
		{
			echo "fail";
		}
	}
}
?>