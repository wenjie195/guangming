

<!doctype html>
<html>

<head>


<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/ewein-city-of-dreams.php" />
<link rel="canonical" href="https://gmvec.com/ewein-city-of-dreams.php" />
<meta property="og:title" content="ewein City of Dreams 梦想之城 | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>ewein City of Dreams 梦想之城 | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">



<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow gold-bg min-height">
	<div class="width100 overflow text-center">
		<a href="https://www.zeon.com.my/projects/info?id=proje6fffdde-dde6-48c3-8c85-b1e99b8cee50" target="_blank"><img src="img/ewein2.png" class="mah-size opacity-hover text-center" alt="ewein City of Dreams 梦想之城" title="ewein City of Dreams 梦想之城"></a>
	</div>
    <div class="width100 overflow margin-top30 first-div-margin">    
        
        <div class="width100 top-video-div overflow">


                            
                                <iframe class="youtube-top-iframe ow-top-iframe" src="https://www.youtube.com/embed/LFzduCN2QWk?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                       <p class="gold-text four-div-p text-center"><a href="https://my.matterport.com/show/?m=WZfy6SNApdB" class="blue-to-orange" target="_blank">360°</a></b></p>

			<!--
            <div class="right-project-div">

                    
                        <a href="" target="_blank"><img src="uploads/" class="project-logo opacity-hover"></a>
            
                        
                                <a href="" target="_blank"><img src="uploads/" class="project-logo opacity-hover "></a>
                          
				</div>-->
        </div>
    </div>

    <div class="clear"></div>
    <!--
    <div class="width100 overflow margin-top30">
    	

                        <div class="two-div  overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/EkMVnIr7HUA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                           
                                    <p class="gold-text four-div-p text-center"><a href="https://ferringhiresidence2.com/360.php" class="blue-to-orange" target="_blank">360°</a></b></p>
                              
                        </div>
                         <div class="two-div second-two-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/Hu4gvtoi-eg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                           
                                    <p class="gold-text four-div-p text-center"><a href="https://ferringhiresidence2.com/360.php" class="blue-to-orange" target="_blank">360°</a></b></p>
                              
                        </div>                  

                    
                   


</div>-->
</div>

<?php include 'js.php'; ?>

</body>
</html>