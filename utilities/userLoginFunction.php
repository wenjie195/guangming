<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Registration.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    if(isset($_POST['login']))
    {
        $username = rewrite($_POST['username']);
        // $phone = rewrite($_POST['phone']);
        $password = $_POST['password'];

        $userRows = getRegistration($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];
    
            $tempPass = hash('sha256',$password);
            $finalPassword = hash('sha256', $user->getSalt() . $tempPass);

                if($finalPassword == $user->getPassword()) 
                // if($phone == $user->getPhoneNo()) 
                {

                    $_SESSION['uid'] = $user->getUid();
                    // $_SESSION['usertype_level'] = $user->getUserType();
                    
                    if($user->getUserType() == 1)
                    {
                        // header('Location: ../liveChat.php');
                        echo "<script>alert('login successfully !!');window.location='../liveChat.php'</script>"; 
                    }
                    else
                    {
                        echo "<script>alert('unknown user type');window.location='../liveChat.php'</script>";
                    }

                }
                else 
                {
                    echo "<script>alert('incorrect password !!');window.location='../liveChat.php'</script>";  
                }
        }
        else
        {
            echo "<script>alert('no user with this username');window.location='../liveChat.php'</script>";  
        }
    }
    $conn->close();
}
?>