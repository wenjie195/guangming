<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $content = $_POST["update_content"];
    $announcementUid = rewrite($_POST["announcement_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $announcementDetails = getAnnouncement($conn," uid = ? ",array("uid"),array($announcementUid),"s");   

    if(!$announcementDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($content)
        {
            array_push($tableName,"content");
            array_push($tableValue,$content);
            $stringType .=  "s";
        }

        array_push($tableValue,$announcementUid);
        $stringType .=  "s";
        $announcementUpdated = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($announcementUpdated)
        {
            header('Location: ../adminViewAnnouncement.php');
        }
        else
        {
            echo "<script>alert('fail to update annoucement !!');window.location='../adminViewAnnouncement.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminViewAnnouncement.php'</script>";  
    }
}
else 
{
    header('Location: ../index.php');
}
?>