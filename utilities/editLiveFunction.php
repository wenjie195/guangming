<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Liveshare.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $updateTitle = rewrite($_POST["update_title"]);
    $updateHost = rewrite($_POST["update_host"]);
    $updatePlatform = rewrite($_POST["update_platform"]);
    $updateLink = rewrite($_POST["update_link"]);

    $liveId = rewrite($_POST["live_id"]);

    $allLive = getLiveShare($conn," WHERE title = ? AND link = ? ",array("title","link"),array($updateTitle,$updateLink),"ss");
    $existingLive = $allLive[0];

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $liveIdDetails = getLiveShare($conn," id = ?   ",array("id"),array($liveId),"s");   
    
    // if(!$existingLive)
    // if(!$liveId)
    // {

        if(!$liveIdDetails)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($updateTitle)
            {
                array_push($tableName,"title");
                array_push($tableValue,$updateTitle);
                $stringType .=  "s";
            }
            if($updateHost)
            {
                array_push($tableName,"host");
                array_push($tableValue,$updateHost);
                $stringType .=  "s";
            }
            if($updatePlatform)
            {
                array_push($tableName,"platform");
                array_push($tableValue,$updatePlatform);
                $stringType .=  "s";
            }
            if($updateLink)
            {
                array_push($tableName,"link");
                array_push($tableValue,$updateLink);
                $stringType .=  "s";
            }
            array_push($tableValue,$liveId);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"live_share"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "UPDATED !!";
                // header('Location: ../adminDashboard.php');
                if(isset($_SESSION['url'])) 
                {
                    $url = $_SESSION['url']; 
                    header("location: $url");
                }
                else 
                {
                    // header("location: $url");
                    header('Location: ../adminDashboard.php');
                }
            }
            else
            {
                echo "FAIL !!";
            }
        }
        else
        {
            echo "GG !!";
        }

    // }
    // else
    // {
    //     echo "Duplicated";
    // }

}
else 
{
    header('Location: ../index.php');
}
?>
