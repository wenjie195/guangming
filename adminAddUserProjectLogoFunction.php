<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function addProjectLogo($conn,$uid,$userUid,$imageName,$type)
{
    if(insertDynamicData($conn,"image",array("uid","user_uid","image_one","type"),
            array($uid,$userUid,$imageName,$type),"sssi") === null)
        {
            return false;
        }
    else
    {  }
    return true;
}

$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

    $uid = md5(uniqid());

	$userUid = $_POST['picUid'];

	// //   FOR DEBUGGING 
	// echo "<br>";
	// echo $uid."<br>";
	// echo $userUid."<br>";

	$_SESSION['pic_link'] = $uid;
	$_SESSION['user_uid'] = $userUid;

	$type = 1;
	
	$image_array_1 = explode(";", $data);
	$image_array_2 = explode(",", $image_array_1[1]);
	$data = base64_decode($image_array_2[1]);
	// $imageName = time() . '.png';
	$imageName = $timestamp . '.png';
	// $imageName = $timestamp.$data.'.png';
	// file_put_contents('userLogo/'.$imageName, $data);
	file_put_contents('uploads/'.$imageName, $data);

	if (addProjectLogo($conn,$uid,$userUid,$imageName,$type))
	{
	?>	
		<div>
			<img src=uploads/<?php echo $imageName  ?> />
		</div>	
		<h4><?php echo "Project Logo Added !!"; ?></h4>
		<!-- <a href="adminAddUserProjectLogoTwo.php">
			<div class="width100 overflow text-center">     
				<button class="clean-button clean login-btn pink-button">Next</button>
			</div>
			<div class="clear">  </div>
		</a> -->

		<form action="utilities/addProjectLinkFunction.php" method="POST">
			<div class="width100 overflow text-center">     

				<div class="dual-input">
					<p class="input-top-text">Project Reference Link</p>
					<input class="aidex-input clean" type="text" placeholder="Project Reference Link" id="register_link" name="register_link" required>        
				</div>

				<div class="clear">  </div>

				<button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
			</div>

			<div class="clear">  </div>
		</form>

		<div class="clear">  </div>
	<?php
	}
	else
	{
		echo "ERROR !!";
	}
}
?>