<?php
if(isset($_SESSION['uid']))
{
?>
    <div class="clear"></div>
    <a href="liveChat.php" class="iframe">
        <img src="img/chat.gif" class="absolute-chat opacity-hover" class='iframe' alt="Live Chat" title="Live Chat">
    </a>
<?php
}
else
{
?>
    <div class="clear"></div>
    <a href="login.php">
        <img src="img/chat.gif" class="absolute-chat opacity-hover" class='iframe' alt="Live Chat" title="Live Chat">
    </a>
<?php
}
?>
<!-- <div class="clear"></div>
<a href="liveChat.php" class="iframe"><img src="img/chat.png" class="absolute-chat opacity-hover" class='iframe' alt="Live Chat" title="Live Chat"></a> -->